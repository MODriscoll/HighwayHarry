﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScenes : MonoBehaviour
{
    // Test variable
    // Naming main game level as number one
    int gameLevel = 1;

    // Declaring variable for the scene manager
    private Scene scene;

    // Start function
    void Start()
    {
        scene = SceneManager.GetActiveScene();
    }

    // Function to load up the first level
    public void LoadMainGame()
    {
        SceneManager.LoadScene(gameLevel);
    }

    // Function to load main menu
    public void LoadMainMenu()
    {
        SceneManager.LoadScene(0);
    }

    // Function to quit the game
    public void QuitGame()
    {
        Application.Quit();
    }

    // Function to reload scene
    public void ReloadScene()
    {
        SceneManager.LoadScene(scene.name);
    }
}

