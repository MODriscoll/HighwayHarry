﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Axle
{
    // The wheels of this axle
    public WheelCollider LeftWheel, RightWheel;

    // If this is a steering axle
    public bool Steers;

    // If this axle applies torque to the wheels
    public bool Torques;

    // Pre-calculated value for speed formula
    public static readonly float sf = 2f * Mathf.PI * 60f / 1000f;
}

[RequireComponent(typeof(Rigidbody))]
public class VehicleActor : MonoBehaviour
{
    // The center of mass for the vehicle
    public Transform CenterOfMass = null;

    // The axles of this vehicle
    public List<Axle> DriveShaft = new List<Axle>();

    [Header("Steering")]
    // The max angle for steering axles
    public float MaxSteeringAngle = 45f;

    // The speed of which steering axle turn
    public float SteeringSpeed = 15f;

    [Header("Torque")]
    // The max torque for the torque axles
    public float MaxTorqueSpeed = 50f;

    // The max speed of the car in total
    public float MaxSpeed = 100f;

    void Start()
    {
        // Has center of mass been set?
        if (CenterOfMass != null)
        {
            // Get the rigidbody component
            Rigidbody rig = GetComponent<Rigidbody>();

            // Set the center of mass
            rig.centerOfMass = transform.InverseTransformPoint(CenterOfMass.position);
        }
    }

    public void Drive()
    {
        // Cycle through each axle
        foreach (Axle axle in DriveShaft)
        {
            // Does this axle apply torque?
            if (axle.Torques)
            {
                // Calculate the speed of this axle
                float speed = axle.LeftWheel.radius * axle.LeftWheel.rpm * Axle.sf;

                // Is speed lower than the max speed?
                if (speed < MaxSpeed)
                {
                    // Apply torque to this axle
                    axle.LeftWheel.motorTorque = MaxTorqueSpeed;
                    axle.RightWheel.motorTorque = MaxTorqueSpeed;
                }
                else
                {
                    // Allow wheels to roll
                    axle.LeftWheel.motorTorque = 0f;
                    axle.RightWheel.motorTorque = 0f;
                }
            }
        }
    }

    public void SteerTowards(Vector3 position)
    {
        // Get the normalized displacement
        Vector3 dis = transform.InverseTransformPoint(position).normalized;

        // Calculate the steering angle
        float angle = dis.x * MaxSteeringAngle;

        // Calculate the turn speed
        float speed = SteeringSpeed * Time.deltaTime;

        // Cycle through each axle
        foreach (Axle axle in DriveShaft)
        {
            // Is this a steering axle?
            if (axle.Steers)
            {
                // Set the angle of this axle
                axle.LeftWheel.steerAngle = Mathf.Lerp(axle.LeftWheel.steerAngle, angle, speed);
                axle.RightWheel.steerAngle = Mathf.Lerp(axle.RightWheel.steerAngle, angle, speed);
            }
        }
    }
}

