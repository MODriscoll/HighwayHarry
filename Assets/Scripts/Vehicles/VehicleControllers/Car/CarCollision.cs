﻿
using UnityEngine;

public class CarCollision : MonoBehaviour {

    // Layers for the car to explode on
    public LayerMask ExplodeOnContact;

    // Reference to the car controller
    private CarController m_Car;

    void Start ()
    {
        // Get reference to the car
        m_Car = GetComponentInParent<CarController>();
	}
	
	void OnCollisionEnter(Collision col)
    {
        // Did the car collide with the player?
        if (col.gameObject.layer == m_Car.m_Player.gameObject.layer)
        {
            // To get average
            Vector3 average = Vector3.zero;

            // Cycle through each contact point
            foreach (ContactPoint point in col.contacts)
            {
                // Add the negated normal to the average
                average += -point.normal;
            }

            // Ram the player
            m_Car.Ram(average.normalized, col.relativeVelocity.magnitude);
        }
        // Can this object cause the car to explode?
        if (((ExplodeOnContact.value >> col.gameObject.layer) & 0x1) != 0)
        {
            // Detonate the car
            m_Car.Detonate();
        }
    }
}
