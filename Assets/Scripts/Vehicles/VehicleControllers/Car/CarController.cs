﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(VehicleActor))]
public class CarController : MonoBehaviour
{
    // How long this car will live for
    public float LifeTime = 10f;

    // Additional force to apply when ramming the player
    public float RammingPower = 10f;

    // Additional up force to apply when ramming the player ( is added to, not scaled )
    public float RammingLiftPower = 1f;

    // The radius of the cars explosion
    public float ExplosionRadius = 5f;

    // The power to apply when exploding
    public float ExplosionPower = 10f;

    // Additional up force to apply when exploding ( is added to, not scaled )
    public float ExplosionLiftPower = 5f;

    // The explosion prefab to spawn
    public GameObject Explosion;

    // Reference to the vehicle actor
    private VehicleActor m_Vehicle;

    // Reference to the player
    [HideInInspector]
    public PlayerController m_Player;

    // Layers to ignore when exploding
    [SerializeField]
    private LayerMask IgnoreWhenExploding;

    // If this car has exploded
    private bool m_Exploded = false;

    void Awake()
    {
        // Load the explosion prefab
        // 

        // Get the vehicle actor
        m_Vehicle = GetComponent<VehicleActor>();
    }

    void Update()
    {
        // Decrease the time
        LifeTime -= Time.deltaTime;

        // Has time run out?
        if (LifeTime <= 0f)
        {
            // Detonate the car
            Detonate();
        }
    }

    void FixedUpdate()
    {
        // Steer the car towards the player
        m_Vehicle.SteerTowards(m_Player.Spine.position);

        // Drive towards the player
        m_Vehicle.Drive();
    }

    #region Functions

    public void Ram(Vector3 normal, float velocity)
    {
        // Get the cars rigidbody
        Rigidbody rigid = GetComponent<Rigidbody>();

        // Scale the normal by the impluse
        normal *= velocity;

        // Apply the additional force
        normal += normal * RammingPower;

        // Apply the upwards force
        normal.y += RammingLiftPower;

        // Ram the player
        m_Player.EnterRagdoll(normal, ForceMode.Impulse);
    }

    public void Detonate()
    {
        // Has car not been detonated yet?
        if (!m_Exploded)
        {
            // Set car to detonated
            m_Exploded = true;

            // Get the direction to the player
            Vector3 dir = (m_Player.Spine.transform.position - transform.position).normalized;

            // Create the new raycast
            Ray ray = new Ray(transform.position, dir);

            // Capture information
            RaycastHit hit;

            // Perform the raycast
            if (Physics.Raycast(ray, out hit, ExplosionRadius, ~IgnoreWhenExploding.value, QueryTriggerInteraction.Ignore))
            {
                // Is the hit object the player
                if (hit.collider.gameObject.layer == m_Player.gameObject.layer)
                {
                    // Get the force to apply
                    Vector3 force = (-hit.normal) * ExplosionPower;

                    // Apply the additional up power
                    force.y += ExplosionLiftPower;

                    // Explode the player
                    m_Player.EnterRagdoll(force, ForceMode.Impulse);
                }
                // Is the hit object another car?
                else if (hit.collider.gameObject.layer == gameObject.layer)
                {
                    // Get the object car controller
                    CarController car = hit.collider.GetComponent<CarController>();

                    // Detonate this car
                    car.Detonate();
                }
            }

            // Create the explosion
            Instantiate(Explosion, transform.position, transform.rotation);

            // Destroy this car
            Destroy(gameObject);
        }
    }

    #endregion

    #region Initialisation

    public void Init(PlayerController player)
    {
        // Save the reference to the player
        m_Player = player;

        // Look at the player
        transform.LookAt(m_Player.transform);

        // Nullify the pitch and roll rotations
        transform.eulerAngles = new Vector3(0f, transform.eulerAngles.y, 0f);
    }

    #endregion


    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, ExplosionRadius);
    }
}
