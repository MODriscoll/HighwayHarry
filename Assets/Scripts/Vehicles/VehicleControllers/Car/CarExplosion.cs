﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
[RequireComponent(typeof(AudioSource))]
public class CarExplosion : MonoBehaviour
{
    // Reference to the particle system
    private ParticleSystem m_Particles;

    // Reference to the audio source
    private AudioSource m_Audio;

	void Start ()
    {
        // Get the needed components
        m_Particles = GetComponent<ParticleSystem>();
        m_Audio = GetComponent<AudioSource>();

        // Start the blow up co-routine
        StartCoroutine(Blowup());
	}

    private IEnumerator Blowup()
    {
        // Play the explosion
        m_Particles.Play();
        m_Audio.Play();

        // Wait until both the audio and particles have finished
        yield return new WaitWhile(() => m_Particles.particleCount > 0 | m_Audio.isPlaying);

        // Destroy this explosion
        Destroy(gameObject);
    }
}
