﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(CharacterController))]
public class PlayerController : MonoBehaviour
{
    // Componenets needed
    [HideInInspector] public Animator m_Animator;
    [HideInInspector] public CharacterController m_Controller;

    [Header("Structure")]

    // The spine of the players model
    public Transform Spine;

    // The objects to hide on ragdoll
    public GameObject[] HideOnRagdoll;

    [Header("Audio")]
    // Audio to play when player enters ragdoll
    public AudioSource PlayWhenRagdoll;
    // Aduio to play when player stops moving after being in ragdoll
    public AudioSource PlayOnDeath;

    [Header("Variables")]

    // Speed of the player
    public float Speed = 10f;

    [Header("Settings")]
    // The delay between freezing after ragdolling and a sore back
    public float OnDeathDelay = 1f;

    // Name of the ragdoll and vehicles layer
    [SerializeField] private string RagdollLayer = "Ragdoll";
    [SerializeField] private string VehicleLayer = "Vehicles";

    // If the player is currently in a ragdoll state
    public bool IsRagdoll { get { return !m_Animator.enabled; } }

    // Name of layers as integers
    private int m_RagLayer, m_VehLayer;

    // Previous axes values
    private float m_PX = 0f, m_PZ = 0f;

    // Reference to the map manager
    public MapManager Map { get; set; }

    // The tolerance for if the player has stopped moving
    public float MovingTolerance = 0.5f;

    void Awake()
    {
        // Get the animator and controller
        m_Animator = GetComponent<Animator>();
        m_Controller = GetComponent<CharacterController>();

        // Get all components with rigidbodies
        // ( This should be every ragdoll with joint )
        Rigidbody[] Rags = GetComponentsInChildren<Rigidbody>();

        // Cycle through each rigidbody
        foreach (Rigidbody rig in Rags)
        {
            // Set the rigidbody to kinematic
            rig.isKinematic = true;
        }

        // Get the layers as integers
        m_RagLayer = LayerMask.NameToLayer(RagdollLayer);
        m_VehLayer = LayerMask.NameToLayer(VehicleLayer);

        // Ignore the ragdoll layer completely, allow vehicles to hit this layer initially
        Physics.IgnoreLayerCollision(gameObject.layer, m_RagLayer, true);
        Physics.IgnoreLayerCollision(m_VehLayer, m_RagLayer, true);
        Physics.IgnoreLayerCollision(gameObject.layer, m_VehLayer, false);
    }

    #region Updates

    void Update()
    {
        // Is the player in control?
        if (!IsRagdoll)
        {
            // Move the player
            Move();
        }
    }

    #endregion

    #region Functions

    private void Move()
    {
        // Get axis values
        float X = InputManager.GetAxis("Horizontal");
        float Z = InputManager.GetAxis("Vertical");

        // Calculate the force to apply
        Vector3 force = (Vector3.forward * Z) + (Vector3.right * X);

        // Scale the force by speed
        force *= Speed;

        // Truncate the force to stay within speed
        force = Vector3.ClampMagnitude(force, Speed);

        // Get the perctange of speed
        float perc = force.sqrMagnitude / (Speed * Speed);

        // Set the animators velocity
        m_Animator.SetFloat("Velocity", perc);

        // Is controller not grounded?
        if (!m_Controller.isGrounded)
        {
            // Apply gravity in-case
            force.y = Physics.gravity.y;
        }

        // Apply force to controller
        m_Controller.Move(force * Time.deltaTime);

        // Get positive axes values
        float posX = Mathf.Abs(X);
        float posZ = Mathf.Abs(Z);

        // Has the character increased speed in either axes?
        if ((posX > 0 && posX >= m_PX) || (posZ > 0 && posZ >= m_PZ))
        {
            // Set the rotation
            transform.localEulerAngles = new Vector3(0f, Mathf.Rad2Deg * Mathf.Atan2(force.x, force.z));
        }

        // Save the positive axes values
        m_PX = posX;
        m_PZ = posZ;
    }

    public void EnterRagdoll(Vector3 force, ForceMode mode)
    {
        // Is animator enabled?
        if (!IsRagdoll)
        {
            // Disable the animator
            // Cycle through each rigidbody
            foreach (Rigidbody rig in EnableRagdoll(false))
            {
                // Set this rigidbody to recieve physics updates
                rig.isKinematic = false;

                // Apply the force
                rig.AddForce(force, mode);
            }

            // Turn off the character colliders hitbox
            m_Controller.detectCollisions = false;

            // Set vehicles to now interact with the ragdoll
            Physics.IgnoreLayerCollision(gameObject.layer, m_VehLayer, true);
            Physics.IgnoreLayerCollision(m_VehLayer, m_RagLayer, false);

            foreach (GameObject Object in HideOnRagdoll)
            {
                Object.SetActive(false);
            }

            // Start the on ragdoll routine
            StartCoroutine(OnRagdoll());
        }
    }

    public void ExitRagdoll()
    {
        // Is animator disabled?
        if (IsRagdoll)
        {
            // Enable the animator
            // Cycle through each rigidbody
            foreach (Rigidbody rig in EnableRagdoll(true))
            {
                // Set this rigidbody to not recieve physics updates
                rig.isKinematic = true;

                // Nullify its velocity
                rig.velocity = Vector3.zero;
            }

            // Turn off the character colliders hitbox
            m_Controller.detectCollisions = true;

            // Set vehicles to ignore the ragdoll
            Physics.IgnoreLayerCollision(gameObject.layer, m_VehLayer, false);
            Physics.IgnoreLayerCollision(m_VehLayer, m_RagLayer, true);

            foreach (GameObject Object in HideOnRagdoll)
            {
                Object.SetActive(true);
            }
        }
    }

    #endregion

    #region Co-Routines

    private IEnumerator OnRagdoll()
    {
        // Get the player to scream
        PlayWhenRagdoll.Play();

        // Get the spines rigidbody
        Rigidbody rigid = Spine.GetComponent<Rigidbody>();

        // Delay between start of ragdoll and to check if sleeping
        float initialdelay = 0.5f;

        // Track the time between states
        float tracker = 0f;

        // Repeat until spines rigidbody is asleep
        while (rigid.velocity.sqrMagnitude > MovingTolerance || tracker < initialdelay)
        {
            // Increment the time
            tracker += Time.deltaTime;

            // Wait till the next frame
            yield return new WaitForEndOfFrame();
        }

        // Reset the time
        tracker = 0f;

        // Repeat until the initial ragdoll has ended
        while (PlayWhenRagdoll.isPlaying) { yield return new WaitForEndOfFrame(); }

        // Repeat until the delay is finished
        while (tracker < OnDeathDelay)
        {
            // Increment the time
            tracker += Time.deltaTime;

            // Wait till the next frame
            yield return new WaitForEndOfFrame();
        }

        // Play the on death sound
        PlayOnDeath.Play();

        // Activate the maps game over
        Map.EndGame(true);
    }

    #endregion

    #region Helpers

    private Rigidbody[] EnableRagdoll(bool enable)
    {
        // Enable/disable the animator
        m_Animator.enabled = enable;

        // Return all the ragdolls rigidbodies
        return GetComponentsInChildren<Rigidbody>();
    }

    #endregion

    #region Funny

    public void Slip()
    {
        // Make the player slip
        EnterRagdoll(m_Controller.velocity, ForceMode.VelocityChange);
    }

    #endregion
}

