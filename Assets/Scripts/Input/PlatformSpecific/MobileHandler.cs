﻿
using UnityEngine;

public class MobileHandler : InputHandler
{
    public MobileHandler()
    {
        // Allow support for custom mouse handler
        SupportsMouseHandler = true;
    }

    public override Vector2 MousePosition { get { return m_Mouse.Position; } }

    public override float GetAxis(string name)
    {
        // Does this axis exist?
        if (m_Axes.ContainsKey(name))
        {
            // Return the value of this axis
            return m_Axes[name].Value;
        }

        // Throw invalid exception
        throw new UnityException(name + " does not exist as an axis");
    }

    public override bool GetButton(string name)
    {
        // Does this button exist?
        if (m_Buttons.ContainsKey(name))
        {
            // Return the state of this button
            return m_Buttons[name].Pressed;
        }

        // Throw invalid exception
        throw new UnityException(name + " does not exist as an button");
    }

    public override bool GetButtonDown(string name)
    {
        // Does this button exist?
        if (m_Buttons.ContainsKey(name))
        {
            // Return the state of this button
            return m_Buttons[name].Down;
        }

        // Throw invalid exception
        throw new UnityException(name + " does not exist as an button");
    }

    public override bool GetButtonUp(string name)
    {
        // Does this button exist?
        if (m_Buttons.ContainsKey(name))
        {
            // Return the state of this button
            return m_Buttons[name].Up;
        }

        // Throw invalid exception
        throw new UnityException(name + " does not exist as an button");
    }
}
