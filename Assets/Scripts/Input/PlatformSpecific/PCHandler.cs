﻿
using UnityEngine;

public class PCHandler : InputHandler
{
    public PCHandler()
    {
        // Don't support the custom mouse handler
        SupportsMouseHandler = false;
    }

    public override Vector2 MousePosition { get { return Input.mousePosition; } }

    public override float GetAxis(string name)
    {
        // Return inputs axis
        return Input.GetAxis(name);
    }

    public override bool GetButton(string name)
    {
        // Return buttons status
        return Input.GetButton(name);
    }

    public override bool GetButtonDown(string name)
    {
        // Return buttons status
        return Input.GetButtonDown(name);
    }

    public override bool GetButtonUp(string name)
    {
        // Return buttons status
        return Input.GetButtonUp(name);
    }
}
