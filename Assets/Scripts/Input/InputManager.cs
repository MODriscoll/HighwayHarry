﻿using System;
using UnityEngine;

// Enum representing supported platforms
public enum PlatformType
{
    PC,
    Console,
    Mobile
}

public static class InputManager
{
    // The platform the inputs is providing
    public static PlatformType Platform { get; private set; }

    // Handler for the platform
    private static InputHandler m_Handler = null;

    static InputManager()
    {
        // Set handler based on platform
        #if UNITY_IOS || UNITY_ANDROID
        m_Handler = new MobileHandler();
        Platform = PlatformType.Mobile;
        #elif UNITY_XBOXONE || UNITY_PS4
        m_Handler = new ConsoleHandler();
        Platform = PlatformType.Console;
        #else
        m_Handler = new PCHandler();
        Platform = PlatformType.PC;
        #endif

        // Throw if no handler made
        if (m_Handler == null) { throw new NotImplementedException("Input handler has not been set"); }
    }

    // Functions for getting and setting handlers
    #region Handlers Gets/Sets

    public static InputHandler.AxisHandler CreateAxisHandler(string name)
    {
        // Create and return new axis
        return m_Handler.CreateAxisHandler(name);
    }

    public static InputHandler.ButtonHandler CreateButtonHandler(string name)
    {
        // Create and return new button
        return m_Handler.CreateButtonHandler(name);
    }

    public static InputHandler.AxisHandler GetAxisHandler(string name)
    {
        // Return the axis handler
        return m_Handler.GetAxisHandler(name);
    }

    public static InputHandler.ButtonHandler GetButtonHandler(string name)
    {
        // Return the button handler
        return m_Handler.GetButtonHandler(name);
    }

    public static InputHandler.MouseHanlder GetMouseHandler()
    {
        // Return the mouse handler
        return m_Handler.GetMouseHandler();
    }

    public static bool RemoveAxisHandler(string name)
    {
        // Remove this axis handler
        return m_Handler.RemoveAxisHandler(name);
    }

    public static bool RemoveButtonHandler(string name)
    {
        // Remove this button handler
        return m_Handler.RemoveButtonHandler(name);
    }
    #endregion

    // Functions for getting values of handler
    #region Value Gets

    public static float GetAxis(string name)
    {
        // Return axis value of handler
        return m_Handler.GetAxis(name);
    }

    public static bool GetButton(string name)
    {
        // Return button value of handler
        return m_Handler.GetButton(name);
    }

    public static bool GetButtonDown(string name)
    {
        // Return button value of handler
        return m_Handler.GetButtonDown(name);
    }

    public static bool GetButtonUp(string name)
    {
        // Return button value of handler
        return m_Handler.GetButtonUp(name);
    }

    public static Vector2 MousePosition { get { return m_Handler.MousePosition; } }

    #endregion
}
