﻿using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

public abstract class InputHandler
{
    // All assigned axis of this handler
    protected Dictionary<string, AxisHandler> m_Axes = new Dictionary<string, AxisHandler>();
    // All assigned buttons of this handler
    protected Dictionary<string, ButtonHandler> m_Buttons = new Dictionary<string, ButtonHandler>();

    // Unique mouse handler of this handler
    protected MouseHanlder m_Mouse = new MouseHanlder();

    // If this handler supports the mouse handler
    [DefaultValue(false)]
    public bool SupportsMouseHandler { get; protected set; }

    // Functions related to handlers
    #region Handler Functions

    // Creates new axis if name has not been used
    // Otherwise returns null
    public AxisHandler CreateAxisHandler(string name)
    {
        // Make sure axis doesn't already exist
        if (m_Axes.ContainsKey(name))
        {
            // Don't create a copy
            return null;
        }
        else
        {
            // Create the new axis
            AxisHandler handler = new AxisHandler(name);

            // Add the handler to the dictionary
            m_Axes.Add(name, handler);

            // Return the handler
            return handler;
        }
    }

    // Creates new button if name has not been used
    // Otherwise returns null
    public ButtonHandler CreateButtonHandler(string name)
    {
        // Make sure button doesn't exist
        if (m_Buttons.ContainsKey(name))
        {
            // Don't create a copy
            return null;
        }
        else
        {
            // Create the new button
            ButtonHandler handler = new ButtonHandler(name);

            // Add the handler to the dictionary
            m_Buttons.Add(name, handler);

            // Return the handler
            return handler;
        }
    }

    // For getting the handler of an axis to manage
    public AxisHandler GetAxisHandler(string name)
    {
        // Does this axis exist?
        if (m_Axes.ContainsKey(name))
        {
            // Return the handler
            return m_Axes[name];
        }

        // Return no handler
        return null;
    }

    // For getting the handler of a button to manage
    public ButtonHandler GetButtonHandler(string name)
    {
        // Does this button exist?
        if (m_Buttons.ContainsKey(name))
        {
            // Return the handler
            return m_Buttons[name];
        }

        // Return no handler
        return null;
    }

    // For gettubg the handler of the mouse to manage
    public MouseHanlder GetMouseHandler()
    {
        // Is mouse handler supported?
        if (SupportsMouseHandler)
        {
            // Return the mouse handler
            return m_Mouse;
        }
        else
        {
            // Don't return the handler
            return null;
        }
    }

    // Returns true if axis was removed
    // Returns false otherwise
    public bool RemoveAxisHandler(string name)
    {
        // Remove this axis
        return m_Axes.Remove(name);
    }

    // Returns true if button was removed
    // Returns false otherwise
    public bool RemoveButtonHandler(string name)
    {
        // Remove the button
        return m_Buttons.Remove(name);
    }

    #endregion

    // Functions for inheriting classes
    #region Abstract Functions

    public abstract float GetAxis(string name);

    public abstract bool GetButton(string name);
    public abstract bool GetButtonDown(string name);
    public abstract bool GetButtonUp(string name);

    public abstract Vector2 MousePosition { get; }

    #endregion

    // Helpers for bindings
    #region Bindings

    public class AxisHandler
    {
        public string Name { get; private set; }

        [DefaultValue(0f)]
        public float Value { get; private set; }

        [DefaultValue(-1f)]
        public float Min { get; set; }
        [DefaultValue(1f)]
        public float Max { get; set; }

        public AxisHandler(string name)
        {
            Name = name;
        }

        public void Set(float value)
        {
            Value = value;
        }

        public void Update(float value)
        {
            Value = Mathf.Clamp(Value + value, Min, Max);
        }
    }

    public class ButtonHandler
    {
        public string Name { get; private set; }

        [DefaultValue(false)]
        public bool Pressed { get; set; }

        public bool Down { get { return m_PressedAt + 1 == Time.frameCount; } }
        public bool Up { get { return m_ReleasedAt + 1 == Time.frameCount; } }

        private int m_PressedAt = -2;
        private int m_ReleasedAt = -2;

        public ButtonHandler(string name)
        {
            Name = name;
        }

        public void Press()
        {
            // Is button not already down?
            if (!Pressed)
            {
                // Set button status to pressed
                Pressed = true;

                // Get time as of now
                m_PressedAt = Time.frameCount;
            }
        }

        public void Release()
        {
            // Is button currently down?
            if (Pressed)
            {
                // Set button status to released
                Pressed = false;

                // Get time as of now
                m_ReleasedAt = Time.frameCount;
            }
        }
    }

    public class MouseHanlder
    {
        [DefaultValue(typeof(Vector2), "Vector2.zero")]
        public Vector2 Position { get; private set; }

        public void Set(Vector2 position)
        {
            Position = position;
        }

        public void Set(float x, float y)
        {
            Position = new Vector2(x, y);
        }

        public void SetX(float x)
        {
            Position = new Vector2(x, Position.y);
        }

        public void SetY(float y)
        {
            Position = new Vector2(Position.x, y);
        }

        public void Update(Vector2 value)
        {
            Position = Position + value;
        }

        public void Update(float x, float y)
        {
            Position = Position + new Vector2(x, y);
        }

        public void UpdateX(float x)
        {
            Position = Position + new Vector2(x, Position.y);
        }

        public void UpdateY(float y)
        {
            Position = Position + new Vector2(Position.x, y);
        }
    }

    #endregion
}