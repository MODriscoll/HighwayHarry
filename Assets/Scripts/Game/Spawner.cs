﻿
using UnityEngine;

public class Spawner : MonoBehaviour
{
    // The object to spawn objects around
    public GameObject SpawnAround;

    // The object to spawn
    public GameObject Spawns;

    // Size of the outer ring
    public float Size = 10f;

    // If the worlds up should be used over the objects to spawn around
    public bool UseWorldUp = true;

    // Layers to check for when spawning in objects
    public LayerMask DontSpawnIn = Physics.AllLayers;

    // Radius of inner and outer ring from spawn around object
    private float m_InRadius = 0f, m_OutRadius = 0f;

    // Calculated bounds of the object to spawns whole hierarchy
    private Bounds m_SpawnsBounds;

    public void Init(Camera camera)
    {
        // Throw if there is nothing to spawn/spawn around
        if (SpawnAround == null) { throw new MissingReferenceException("Nothing to spawn objects around"); }
        if (Spawns == null) { throw new MissingReferenceException("Nothing to use to apply extra offset"); }

        // Initialise the ring of the spawner
        InitRing(camera);

        // Initialise the spawn object
        InitSpawnObject();
    }

    public GameObject Spawn()
    {
        // Get reference to transform
        Transform trans = SpawnAround.transform;

        // Add the random yaw direction
        Vector3 dir = RandomDirection();

        // Get a random point with the ring
        Vector3 point = trans.position + (dir * Random.Range(m_InRadius, m_OutRadius));

        // Add the Y extents of the object to spawn
        point.y += m_SpawnsBounds.extents.y;

        // Is the point valid?
        if (ValidatePoint(point))
        {
            // Create and return the new object
            return Instantiate(Spawns, point, Quaternion.identity);
        }
        else
        {
            // Return point not valid
            return null;
        }
    }

    #region Initialising

    private void InitSpawnObject()
    {
        // Create empty bounding box
        m_SpawnsBounds = new Bounds(Spawns.transform.position, Vector3.zero);

        // Get reference to objects renderers
        Renderer[] renderers = Spawns.GetComponentsInChildren<Renderer>();

        // End function here if there are no renderers
        if (renderers.Length == 0) { return; }

        // Cycle through each bouding box
        foreach (Renderer ren in renderers)
        {
            // Encapsulate the childs bounds
            m_SpawnsBounds.Encapsulate(ren.bounds);
        }

        // Get the magnitude of the extents
        float mag = m_SpawnsBounds.extents.magnitude;

        // Apply the additional offset to the ring
        m_InRadius += mag;
        m_OutRadius += mag;
    }

    private void InitRing(Camera camera)
    {
        // Get reference to objects transform
        Transform trans = SpawnAround.transform;

        // To capture directions
        Vector3[] directions = new Vector3[4];

        // Get half of cameras fov
        float halfFOV = camera.fieldOfView * 0.5f;

        // Get yaw and pitch
        Quaternion up = Quaternion.Euler(halfFOV, 0f, 0f);
        Quaternion down = Quaternion.Euler(-halfFOV, 0f, 0f);
        Quaternion right = Quaternion.Euler(0f, halfFOV, 0f);
        Quaternion left = Quaternion.Euler(0f, -halfFOV, 0f);

        // Get bounds of vision directions
        directions[0] = (camera.transform.rotation * down * left) * Vector3.forward;
        directions[1] = (camera.transform.rotation * down * right) * Vector3.forward;
        directions[2] = (camera.transform.rotation * up * left) * Vector3.forward;
        directions[3] = (camera.transform.rotation * up * right) * Vector3.forward;

        // Create a surface to compare against
        Plane surface = new Plane(UseWorldUp ? Vector3.up : trans.up, trans.position);

        // For raycasts
        float len;

        // Default furthest distance to zero
        float fur = 0f;

        // Cycle through each direction
        foreach (Vector3 dir in directions)
        {
            // Create the new raycast
            Ray ray = new Ray(camera.transform.position, dir);

            // Make sure ray isn't parallel
            if (surface.Raycast(ray, out len))
            {
                // Get the distance
                float dis = Vector3.Distance(ray.GetPoint(len), trans.position);

                // Is distance greater than current furthest?
                if (dis > fur)
                {
                    // Set the new furthest
                    fur = dis;
                }
            }
        }

        // Set the min and max radius
        m_InRadius = fur;
        m_OutRadius = fur + Size;
    }

    #endregion

    #region Helpers

    private Vector3 RandomDirection()
    {
        // Get reference to transform
        Transform trans = SpawnAround.transform;

        // Get a random yaw rotation
        Quaternion yaw = Quaternion.Euler(0f, Random.Range(0f, 359f), 0f);

        // Get the forward facing as a quaternion
        Quaternion forward = UseWorldUp ? Quaternion.Euler(Vector3.forward) : Quaternion.Euler(trans.forward);

        // Add the random rotation and return it as a vector
        return (forward * yaw) * Vector3.forward;
    }

    private bool ValidatePoint(Vector3 point)
    {
        // Return if any collider of set layers is within this spawn object
        return Physics.OverlapBox(point, m_SpawnsBounds.extents, 
            Quaternion.identity, DontSpawnIn, QueryTriggerInteraction.Ignore).Length == 0;
    }

    #endregion

    private void OnDrawGizmos()
    {
       //Init(Camera.main);

        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(SpawnAround.transform.position, m_InRadius);

        Gizmos.color = Color.magenta;
        Gizmos.DrawWireSphere(SpawnAround.transform.position, m_OutRadius);
    }
}

