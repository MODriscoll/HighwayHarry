﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapManager : MonoBehaviour
{
    [Header("Game")]
    // The time the player has to collect all the pickups
    public float RoundTime = 300f;

    // Intervals between spawning new cars
    public float SpawnInterval = 1f;

    // The max amount of time before popping the game over screen after player has died
    public float WaitForGameOver = 5f;

    [Header("Map")]
    // The parent of the maps possible pickup locations
    public Transform PickupSpots;

    // The amount of pickups that need to be collected
    public int PickupAmount;

    // The prefab of the pickup to spawn
    public GameObject PickupPrefab;

    // The parent of the possible spawning locations for the player
    public Transform SpawnInSpots;

    [Header("Camera")]
    // Reference to the camera components
    public CameraReference GameCamera;

    // Initial and ragdoll offset for the cameras spring arm when player is in ragdoll
    public Vector3 AliveOffset = Vector3.zero, DeadOffset = Vector3.zero;

    [Header("Player")]
    // Reference to the player
    public PlayerController Player;

    [Header("HUD")]
    // The hud to display during the game
    public Canvas GameHUD;

    // The hud to display when game is over
    public Canvas GameOverHUD;

    // The game huds score text element
    public Text GameScoreHUD;

    // The huds time text element
    public Text GameTimeHUD;

    // The huds outcome text element
    public Text OverOutcomeHUD;

    // The game overs huds score text element
    public Text OverScoreHUD;

    // List of all the pickups spawned
    private List<GameObject> m_Pickups;

    // List of all the cars currently alive
    private List<GameObject> m_Cars = new List<GameObject>();

    // The time when the round started
    private float m_RoundStart;

    // Elapsed time since last enemy spawn
    private float m_ElapsedSpawnTime;

    // The amount of pickups collected
    private int m_Collected;

    // The time since the player has died
    private float m_BeenDeadFor;

    // If the player has been flagged as dead
    private bool m_DeathFlag;

	void Start ()
    {
        // Set the map for the player
        Player.Map = this;

        // Initialise
        Init();
	}
	
	void Update ()
    {
        // Is player alive?
        if (!Player.IsRagdoll)
        {
            // Perform living update
            AliveUpdate();
        }
        else
        {
            // Perform dead update
            DeadUpdate();
        }
    }

    private void AliveUpdate()
    {
        // Get the rounds current time
        float time = Time.time - m_RoundStart;

        // Clamp the time
        time = Mathf.Clamp(time, 0f, RoundTime);

        // Has time run out, or has the player collected all pickups?
        if (time == RoundTime || EndGame())
        {
            // Nullify the time
            GameTimeHUD.text = string.Format("Time: {0:0.00}", 0f);

            // Kill the player
            Player.Slip();
        }
        else
        {
            // Set the time remaining in the hud element
            GameTimeHUD.text = string.Format("Time: {0:0.00}", RoundTime - time);

            // Increase elapsed time
            m_ElapsedSpawnTime -= Time.deltaTime;

            // Has time elapsed?
            if (m_ElapsedSpawnTime < 0f)
            {
                // Spawn a new car
                Spawn();
            }
        }
    }

    private void DeadUpdate()
    {
        // Increment the time since death
        m_BeenDeadFor += Time.deltaTime;

        // Lerp the camera closer 
        GameCamera.Boom.Offset = Vector3.Lerp(AliveOffset, DeadOffset, Mathf.Clamp01(m_BeenDeadFor));

        // Has time surpassed the max wait limit and player has not been flagged as dead?
        if (m_BeenDeadFor >= WaitForGameOver && !m_DeathFlag)
        {
            // Force the game over
            EndGame(true);
        }
    }

    #region Functions
    
    private void Spawn()
    {
        // Spawn the new car
        GameObject obj = GameCamera.Spawner.Spawn();

        // Was an object successfully spawned?
        if (obj != null)
        {
            // Get the car controller
            CarController car = obj.GetComponent<CarController>();

            // Initialise the car
            car.Init(Player);

            // Add the car to the list
            m_Cars.Add(obj);

            // Reset the elapsed time
            m_ElapsedSpawnTime = SpawnInterval;
        }
    }

    // For cars to call
    public void DestroyCar(GameObject obj)
    {
        // Find this car in the list
        if (m_Cars.Contains(obj))
        {
            // Get the index of this object
            int index = m_Cars.IndexOf(obj);

            // Destroy this gameobject
            Destroy(obj);

            // Remove the index from the list
            m_Cars.RemoveAt(index);
        }
    }

    // For pickups to call
    public void DestroyPickup(GameObject obj)
    {
        // Find this pickup in the list
        if (m_Pickups.Contains(obj))
        {
            // Add to score
            GameScoreHUD.text = string.Format("Score: {0}", ++m_Collected);

            // Get the index of this object
            int index = m_Pickups.IndexOf(obj);

            // Destroy this gameobject
            Destroy(obj);

            // Remove the index from the list
            m_Pickups.RemoveAt(index);
        }
    }

    public bool EndGame(bool force = false)
    {
        // End function here if already flagged as dead
        if (m_DeathFlag) { return true; }

        // Get if player has collected all pickups
        bool all = m_Collected == PickupAmount;

        // Is game over being forced or has all pickups been collected?
        if (force || all)
        {
            // Set the outcome text based on if all pickups were collected
            OverOutcomeHUD.text = all ? "You win!" : "You lose...";

            // Set the score text
            OverScoreHUD.text = string.Format("Score: {0} / {1}", m_Collected, PickupAmount);

            // Deactivate the game hud and activate the game over hud
            GameHUD.gameObject.SetActive(false);
            GameOverHUD.gameObject.SetActive(true);

            // Stop any co-rotuines he player might be running
            Player.StopAllCoroutines();

            // Flag the players death
            m_DeathFlag = true;

            // Return game has ended
            return true;
        }

        // Return game is still active
        return false;
    }

    #endregion

    #region Initialisation

    public void Init()
    {
        // Initialise each component
        InitCamera();
        InitPlayer();
        InitPickups();
        InitGame();
    }

    private void InitPlayer()
    {
        // Put the player out of ragdoll state
        Player.ExitRagdoll();

        // Get a random spawn spot from the possible spawn locations
        Transform spot = SpawnInSpots.GetChild(Random.Range(0, SpawnInSpots.childCount - 1));

        // Set the players position and rotation
        Player.transform.SetPositionAndRotation(spot.position, spot.rotation);
    }

    private void InitPickups()
    {
        // Log a warning if the set amount to collect is greater than the set amount of spawn
        if (PickupSpots.childCount < PickupAmount) { Debug.LogWarning("There are not enough pickup spawns to support the amount to collect"); }

        // Get a clamped clone of the pickup amount
        int clamped = Mathf.Clamp(PickupAmount, 0, PickupSpots.childCount);

        // Resize the list to fit requested amount
        m_Pickups = new List<GameObject>(clamped);

        // Get all the possible pickup locations as a list
        List<Transform> spots = new List<Transform>(PickupSpots.GetComponentsInChildren<Transform>());

        // Remove the parent from the list
        spots.RemoveAt(0);

        // Cycle through the amount to spawned
        while (--clamped >= 0)
        {
            // Generate a random index
            int index = Random.Range(0, spots.Count);

            // Get reference to the transform
            Transform trans = spots[index].transform;

            // Generate a new pickup at this location
            GameObject obj = Instantiate(PickupPrefab, trans.position, trans.rotation);

            // Get the pickup component
            PizzaPickup pick = obj.GetComponent<PizzaPickup>();

            // Initialise the pickup
            pick.Init(this);

            // Add the object to the list
            m_Pickups.Add(obj);

            // Remove the transform from the list
            spots.RemoveAt(index);
        }
    }

    private void InitCamera()
    {
        // Quickly attach the camera to the players core
        GameCamera.Boom.AttachedTo = Player.transform;

        // Set the camera to its initial offset
        GameCamera.Boom.Offset = AliveOffset;

        // Apply the changes
        GameCamera.Boom.Apply();

        // Initialise the spawner
        GameCamera.Spawner.Init(GameCamera.Camera);

        // Re-attach the camera to the spine of the player
        GameCamera.Boom.AttachedTo = Player.Spine;
    }

    private void InitGame()
    {
        // Reset the games variables
        m_ElapsedSpawnTime = SpawnInterval;
        m_BeenDeadFor = 0f;
        m_DeathFlag = false;

        m_Collected = 0;

        // Set round to start now
        m_RoundStart = Time.time;

        // Reset the hud elements
        GameScoreHUD.text = string.Format("Score: {0}", 0f);
        GameTimeHUD.text = string.Format("Time: {0:0.00}", RoundTime);

        // Disable the game over hud and enable the game hud
        //GameHUD.gameObject.SetActive(true);
        GameOverHUD.gameObject.SetActive(false);
    }

    #endregion

    #region Resets

    public void Fresh()
    {
        // Start a fresh round
        StartCoroutine(FreshRound());
    }

    private IEnumerator FreshRound()
    {
        // Wait till end of frame
        yield return new WaitForEndOfFrame();

        // Reset then initialise
        Reset();
        Init();
    }

    public void Reset()
    {
        // Reset components
        ResetPickups();
        ResetCars();
    }

    private void ResetPickups()
    {
        // Cycle through any remaining pickups
        foreach (GameObject pick in m_Pickups)
        {
            // Destroy the pickup
            Destroy(pick);
        }

        // Clear the list
        m_Pickups.Clear();
    }

    private void ResetCars()
    {
        // Cycle through any remaining cars
        foreach (GameObject car in m_Cars)
        {
            // Destroy the car
            Destroy(car);
        }

        // Clear the list
        m_Cars.Clear();
    }

    #endregion
}
