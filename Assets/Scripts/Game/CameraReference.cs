﻿
using UnityEngine;

[RequireComponent(typeof(Camera))]
[RequireComponent(typeof(SpringArm))]
[RequireComponent(typeof(Spawner))]
public class CameraReference : MonoBehaviour {

    // Reference to each component
    public Camera Camera { get; private set; }
    public SpringArm Boom { get; private set; }
    public Spawner Spawner { get; private set; }

    void Awake()
    {
        // Get and set each component
        Camera = GetComponent<Camera>();
        Boom = GetComponent<SpringArm>();
        Spawner = GetComponent<Spawner>();
    }
}
