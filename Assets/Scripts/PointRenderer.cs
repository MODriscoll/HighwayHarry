﻿
using System.Collections.Generic;
using UnityEngine;

public class PointRenderer : MonoBehaviour
{
    // The radius to draw the points
    public float Radius = 1f;

    // Colour to draw the points
    public Color Colour = Color.white;

	void OnDrawGizmos()
    {
        // Get all children of this object
        List<Transform> points = new List<Transform>(GetComponentsInChildren<Transform>());

        // Remove this object from the list
        points.RemoveAt(0);

        // Set the render colour
        Gizmos.color = Colour;

        // Cycle through every point
        foreach (Transform p in points)
        {
            // Draw this point
            Gizmos.DrawWireSphere(p.position, Radius);
        }
    }
}
