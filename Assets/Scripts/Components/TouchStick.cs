﻿
using UnityEngine;
using UnityEngine.EventSystems;

public class TouchStick : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler
{
    // How far out the joystick can go
    public float Reach;

    // What range the joystick will lose focus
    public float SnapAt = Mathf.Infinity;

    // If joystick should immediately snap back to the center or lerp back
    public bool InstantCenter = true;

    // Name of the axes this joystick will set
    public string VerticalAxisName = "Vertical", HorizontalAxisName = "Horizontal";

    // References to the vertical and horizontal mobile axis   
    private InputHandler.AxisHandler m_VerticalAxis;
    private InputHandler.AxisHandler m_HorizontalAxis;

    // Center of the joystick as a vector2
    private Vector2 m_Center;

    // If the joystick has been snapped
    private bool m_Snapped = true;

    // Value when lerping joystick to center
    private float m_SnapBack = 1f;

    void Awake()
    {
        // Create the axes
        CreateAxes();

        // Make sure the snap range is not less than the reach
        if (SnapAt < Reach) { SnapAt = Reach * 5f; }

        // Get the position as a vector2
        m_Center = transform.position;
    }

    void OnDisable()
    {
        // End function here if mobile isn't active
        if (InputManager.Platform != PlatformType.Mobile) { return; }

        // Re-center the joystick
        transform.position = m_Center;

        // Nullify the axes values
        m_VerticalAxis.Set(0f);
        m_HorizontalAxis.Set(0f);
    }

    void Update()
    {
        // Has joystick snapped?
        if (m_Snapped && m_SnapBack != 1f)
        {
            // Increment the snap back value
            m_SnapBack = Mathf.Clamp01(m_SnapBack + Time.deltaTime);

            // Lerp the joystick back
            transform.position = Vector2.Lerp(transform.position, m_Center, m_SnapBack);

            // Lerp the axes back
            m_VerticalAxis.Set(Mathf.Lerp(m_VerticalAxis.Value, 0f, m_SnapBack));
            m_HorizontalAxis.Set(Mathf.Lerp(m_HorizontalAxis.Value, 0f, m_SnapBack));
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        // Set to unsnapped
        m_Snapped = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        // Is joystick still in control?
        if (!m_Snapped)
        {
            // Get the new position
            Vector2 New = eventData.position;

            // Get the displacement between point and center
            Vector2 Dis = New - m_Center;

            // Should the joystick snap?
            if (Dis.magnitude > SnapAt)
            {
                // Snap off
                OnPointerUp(eventData);

                // End this function
                return;
            }

            // Clamp the displacement
            Dis = Vector2.ClampMagnitude(Dis, Reach);

            // Set the joysticks position
            transform.position = m_Center + Dis;

            // Re-calculate the displacement
            Dis = m_Center - (Vector2)transform.position;

            // Get normalized perctange of reach
            Dis /= Reach;

            // Need to negate both axes before applying
            Dis = -Dis;

            // Set the axes values
            m_VerticalAxis.Set(Dis.y);
            m_HorizontalAxis.Set(Dis.x);
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        // Set joystick to snapped
        m_Snapped = true;

        // Is joystick set to snap back instantly?
        if (InstantCenter)
        {
            // Center the joystick
            transform.position = m_Center;

            // Nullify axes
            m_VerticalAxis.Set(0f);
            m_HorizontalAxis.Set(0f);

            // Set snap back to full
            m_SnapBack = 1f;
        }
        else
        {
            // Reset snap back value
            m_SnapBack = 0f;
        }
    }

    private void CreateAxes()
    {
        // Get the axes handlers
        m_VerticalAxis = InputManager.GetAxisHandler(VerticalAxisName);
        m_HorizontalAxis = InputManager.GetAxisHandler(HorizontalAxisName);

        // Create the new handlers if they don't exist already
        if (m_VerticalAxis == null) { m_VerticalAxis = InputManager.CreateAxisHandler(VerticalAxisName); }
        if (m_HorizontalAxis == null) { m_HorizontalAxis = InputManager.CreateAxisHandler(HorizontalAxisName); }
    }
}

