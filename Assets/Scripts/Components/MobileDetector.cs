﻿
using UnityEngine;

public class MobileDetector : MonoBehaviour
{
    void Start()
    {
        // Enable this object if mobile is active
        gameObject.SetActive(InputManager.Platform == PlatformType.Mobile);
    }
	
}
