﻿
using UnityEngine;

public class SpringArm : MonoBehaviour
{
    // The object the spring is attached to    
    public Transform AttachedTo = null;

    // What rotations should be inherited from the attached object
    public bool RotateRoll = false, RotatePitch = false, RotateYaw = true;

    // The maximum length from the attached object
    public float Length = 0f;

    // Offset to be applied to this object
    public Vector3 Offset = Vector3.zero;

    // Optional layermask for raycasting
    public LayerMask Ignore;

    void Start()
    {
        // Automatically apply
        Apply();
    }

    void LateUpdate()
    {
        // Assure there is a attached object
        if (AttachedTo == null) { return; }

        // Apply actual position
        Apply();

        // Get direction between objects
        Vector3 dir = transform.position - AttachedTo.position;

        // Capture the length
        float len = dir.magnitude;

        // Normalise direction
        dir.Normalize();

        // Create the raycast to test for collision
        Ray ray = new Ray(AttachedTo.position, dir);

        // Capture raycast info
        RaycastHit hit;

        // Perform the raycast, was something intersected?
        if (Physics.Raycast(ray, out hit, len, ~Ignore.value, QueryTriggerInteraction.Ignore))
        {
            // Apply using the intersection point
            Apply(hit.point, true);
        }
    }

    public void Apply()
    {
        // Is there an attached object?
        if (AttachedTo != null)
        {
            // Apply spring
            Apply(AttachedTo.position + (AttachedTo.forward * -Length));
        }
    }

    // Assumes AttachedTo is not null
    private void Apply(Vector3 Point, bool Hit = false)
    {
        // Apply the spring to the object
        transform.position = Point;

        // Was spring not hit?
        if (!Hit)
        {
            // Attach the offset to the position aswell
            transform.Translate(transform.TransformDirection(Offset), Space.World);
        }

        // Set initial rotation to apply
        Vector3 apply = transform.eulerAngles;

        // Is roll rotation being applied?
        if (RotateRoll)
        {
            // Set roll to match attacheds roll
            apply.z = AttachedTo.eulerAngles.z;
        }
        // Is pitch rotation being applied?
        if (RotatePitch)
        {
            // Set pitch to match attacheds pitch
            apply.x = AttachedTo.eulerAngles.x;
        }
        // Is yaw rotation being applied?
        if (RotateYaw)
        {
            // Set yaw to match attacheds yaw
            apply.y = AttachedTo.eulerAngles.y;
        }

        // Set rotation
        transform.eulerAngles = apply;
    }

    #region Helpers

    void OnDrawGizmos()
    {
        // Only peform if enabled
        if (enabled && AttachedTo != null)
        {
            // Perform normal update 
            LateUpdate();

            // Draw the spring to screen
            Gizmos.color = Color.red;
            Gizmos.DrawLine(transform.position, AttachedTo.position);
        }
    }

    #endregion
}
