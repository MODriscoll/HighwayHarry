﻿
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MenuNavigator : MonoBehaviour {

    // List of all the buttons in this menu
    public List<Button> Buttons = new List<Button>();

    // Current index of the menu
    private int m_Index;

	void Start ()
    {
        // Disable this script if not on console
        if (InputManager.Platform == PlatformType.Mobile) { enabled = false; return; }

        // Disable this script if their are no buttons
        if (Buttons.Count == 0) { enabled = false; return; }

        // Activate initial button
        OnEnable();
    }

    void OnEnable()
    {
        // Default the index
        m_Index = 0;

        // Create new pointer event
        PointerEventData eve = new PointerEventData(EventSystem.current);

        // Invoke the on exit for this button
        ExecuteEvents.Execute(Buttons[m_Index].gameObject, eve, ExecuteEvents.pointerEnterHandler);
    }
	
	void Update ()
    {
        // Was the submit button pressed
        if (InputManager.GetButtonDown("DPadSubmit"))
        {
            // Activate this buttons events
            Buttons[m_Index].onClick.Invoke();
        }   
        // Should selected moved up?
        else if (InputManager.GetButtonDown("DPadLeft"))
        {
            // Move to the left
            JumpLeft();
        }
        // Should selected move left
        else if (InputManager.GetButtonDown("DPadRight"))
        {
            // Move to thr right
            JumpRight();
        }
    }

    private void JumpLeft()
    {
        // End function if only one button exists
        if (Buttons.Count == 1) { return; }

        // Create new pointer event
        PointerEventData eve = new PointerEventData(EventSystem.current);

        // Invoke the on exit for this button
        ExecuteEvents.Execute(Buttons[m_Index].gameObject, eve, ExecuteEvents.pointerExitHandler);

        // Is this the first button?
        if (m_Index == 0)
        {
            // Wrap around
            m_Index = Buttons.Count - 1;
        }
        else
        {
            // Subtract by one
            --m_Index;
        }

        // Invoke the on exit for this button
        ExecuteEvents.Execute(Buttons[m_Index].gameObject, eve, ExecuteEvents.pointerEnterHandler);
    }

    private void JumpRight()
    {
        // End function if only one button exists
        if (Buttons.Count == 1) { return; }

        // Create new pointer event
        PointerEventData eve = new PointerEventData(EventSystem.current);

        // Invoke the on exit for this button
        ExecuteEvents.Execute(Buttons[m_Index].gameObject, eve, ExecuteEvents.pointerExitHandler);

        // Is this the last button?
        if (m_Index == Buttons.Count - 1)
        {
            // Wrap around
            m_Index = 0;
        }
        else
        {
            // Increment by one
            ++m_Index;
        }

        // Invoke the on exit for this button
        ExecuteEvents.Execute(Buttons[m_Index].gameObject, eve, ExecuteEvents.pointerEnterHandler);
    }
}
