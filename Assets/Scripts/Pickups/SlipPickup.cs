﻿
using System;
using UnityEngine;

public class SlipPickup : PickupActor {

    public override void OnPickup(GameObject obj)
    {
        // Get player controller from object
        PlayerController player = obj.GetComponent<PlayerController>();

        // Get the player to slip
        player.Slip();

        // Destroy this game object
        Destroy(gameObject);
    }
}
