﻿
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class PickupTrigger : MonoBehaviour
{
    // Reference to the pickup actor
    private PickupActor m_Actor;

    void Start()
    {
        // Get the actor
        m_Actor = GetComponentInParent<PickupActor>();
    }

    void OnTriggerEnter(Collider col)
    {
        // Is the collider allowed to pick up?
        if (((1 << col.gameObject.layer) & m_Actor.CanPickup.value) != 0)
        {
            // Call the on pickup event
            m_Actor.OnPickup(col.gameObject);
        }
    }
}
