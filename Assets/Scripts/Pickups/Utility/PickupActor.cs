﻿
using UnityEngine;

public abstract class PickupActor : MonoBehaviour {

    // The layers allowed to trigger this pickup
    public LayerMask CanPickup;

    // Function to be called when picked up
    public abstract void OnPickup(GameObject obj);
}
