﻿
using UnityEngine;

public class PizzaPickup : PickupActor
{
    // Reference to the meshes root
    public Transform MeshRoot;

    // Turn speed of the pizza
    public float Speed = 90f;

    // Reference to the map
    private MapManager m_Map;

    [SerializeField]
    // The sound to play when picked up
    private AudioSource PickupSound;

    private void Update()
    {
        // Rotate the mesh
        MeshRoot.Rotate(0f, Speed * Time.deltaTime, 0f, Space.Self);
    }

    public void Init(MapManager map)
    {
        // Save reference to the map
        m_Map = map;
    }

    public override void OnPickup(GameObject obj)
    {
        // Player the pickup sound
        PickupSound.Play();

        // Get the map to destroy this object
        m_Map.DestroyPickup(gameObject);
    }
}
