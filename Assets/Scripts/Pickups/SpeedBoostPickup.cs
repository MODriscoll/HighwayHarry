﻿using System.Collections;
using UnityEngine;

public class SpeedBoostPickup : PickupActor {

    // Multiplier of the original speed
    public float Multiplier = 2f;

    // The amount of time the speed boost lasts for
    public float LastsFor = 10f;

    public override void OnPickup(GameObject obj)
    {
        // Get the pizza guy from this game object
        PlayerController player = obj.GetComponent<PlayerController>();

        // Start applying the speed boost
        player.StartCoroutine(ApplyBoost(player));

        // Cycle through every trigger
        foreach (Collider col in GetComponentsInChildren<Collider>())
        {
            // Disbale the trigger
            col.enabled = false;
        }
    }

    public IEnumerator ApplyBoost(PlayerController player)
    {
        // Get the speed to increase by
        float increase = (player.Speed * Multiplier) - player.Speed;

        // Increase the speed of the guy
        player.Speed += increase;

        // Wait for the powers length
        yield return new WaitForSeconds(LastsFor);

        // Decrease back to original speed
        player.Speed -= increase;

        // Destroy this game object
        Destroy(gameObject);
    }
}
